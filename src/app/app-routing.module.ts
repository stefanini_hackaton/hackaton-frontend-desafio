import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientDashboardComponent} from 'app/components/client-dashboard.component';
import {ClientRegisterComponent} from 'app/components/client-register.component';

const routes : Routes = [
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
    }, {
        path: 'dashboard',
        component: ClientDashboardComponent
    }, {
        path: 'clientRegister',
        component: ClientRegisterComponent
    }, {
        path: 'detail/:clientId',
        component: ClientRegisterComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}