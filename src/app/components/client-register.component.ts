import {Component, Input, OnInit} from '@angular/core';
import { Client } from './client';
import { Location } from '@angular/common';
import { ClientService } from 'app/services/client.service';
import { Message } from 'app/components/message';
import { ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'app-client-register',
    templateUrl: './client-register.component.html',
    styleUrls: ['./client-register.component.css']
  })

  export class ClientRegisterComponent implements OnInit {

    @Input() client: Client = new Client();
    returnMsg: Message;

    constructor(
        private location: Location,
        private route: ActivatedRoute,
        private clientService: ClientService
    ) {}

    ngOnInit(): void {
        this.route.params.subscribe((params: Params) => {
            /*Nesta parte do código será implementada a parte referente
              a recuperação do parâmetro de ID do cliente enviado na URL
              e será chamado o serviço que irá realizar a busca da informações
              do cliente relacionado ao ID 
            */
          });
    }

    goBack(): void {
        this.location.back();
    }

    saveClient(): void {
        /* 
            Nesta parte do código deverá ser verificado quando será realizado um 
            SAVE ou um UPDATE de um cliente

            Neste caso deve-se verificar se o cliente é um cliente já existente
            ou um novo cliente e, baseado nesta escolha, deve-se chamado o serviço de
            salvar(saveClient) ou atualizar(updateClient) um cliente
        */
    }

  }
