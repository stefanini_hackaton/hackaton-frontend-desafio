import { HackatonFrontPage } from './app.po';

describe('hackaton-front App', function() {
  let page: HackatonFrontPage;

  beforeEach(() => {
    page = new HackatonFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
