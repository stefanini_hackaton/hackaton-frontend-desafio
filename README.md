# HackatonFrontEnd

Este projeto visa disponibilizar a parte de Front-End para a aplicação de registro de cliente para utilização no Hackaton

## Development server

Execute o comando `ng serve` para iniciar o servidor em modo `desenvolvimento`. Acesse a página `http://localhost:4200/` para visualizar a aplicação.
As alterações realizadas na aplicação são automaticamente carregadas.

## Build

Execute o comando `ng build` para construir (build) do proeto. Os artefatos de construção estarão na pasta `dist/`.

## Desafio 1

Para o desafio 1, será necessário uma série de passos para que se possa redirecionar o cliente selecionado no dashboard para a tela de edição. 

### Tela `client-dashboard.component.html`
Para realizar o direcionamento para a rota correta, iremos utilizar uma diretriz do Angular chamada `[routerLink]`, que irá realizar o direcionamento para a URL de roteamento passando como parâmetro o ID do cliente selecionado. Este comando é inserido diretamente na TAG HTML a qual deseja-se realizar o direcionamento.

A especificação desta diretriz é: [routerLink]= " [<URL de Roteamento>, <PARÂMETRO>] "
### Componente `client-register.component.ts`
Neste component, iremos realizar a chamada ao serviço que retorna um cliente utilizando seu ID como parâmetro. O serviço de cliente já está injetado pelo construtor da classe, através do parâmetro `ClientService`.
```java
constructor(
        private location: Location,
        private route: ActivatedRoute,
        private clientService: ClientService
    ) {}
```
Neste caso, será necessário realizar uma chamada ao método do serviço _getClientById_. Assim que a chamada for realizada, uma forma de se trabalhar o conteúdo da resposta do serviço é através de expressões _lambda_.

```(<variavel de retorno> => this.<variavel loca> = <variavel de retorno>)```

Existem algumas formas diferentes de se trabalhar o retorno dos serviços em Angular, sendo que a utilizada neste projeto é a abordagem de _Promises_. As _Promises_ são classes que representam uma promessa futura de dados a serem utilizadas. Devido a natureza assíncrona do Angular com o Backend, a _Promise_ é uma boa opção. Neste caso, a _Promise_ possui o método _then_ que permite utilizar uma expressão _lambda_ para se trabalhar o retorno do serviço. 

```servico.metodo(param).then(retorno => variável_local = retorno);```

Um ponto importante é que, para se usar o serviço de _getClientById_, deve-se ter o ID do cliente em mãos. Para isso, pode-se recuperar esta informação da opção _params_ presente na rota

```const valor = params['<nome do parâmetro>']```

### Serviço `client.service.ts`

Nesta parte do código, iremos realizar a chamada ao serviço que irá realizar o Update do cliente na base de dados, chamando o backend para tal realização. Seguindo as normas do HTTP, as ações HTTP tem a seguinte funcionalidade:
* POST -> Gravação
* PUT -> Atualização
* GET -> Recuperação de Dados
* DELETE -> Remoção

Como estamos trabalhando com atualização do cliente, devemos utilizar o método **PUT** para tal ação, lembrando que mudando a operação do HTTP não é necessário alterar a URl do serviço. Normalmente, no backend, usa-se a mesma URL para operações HTTP diferentes. Esta é uma forma de se normalizar e reaproveitar as URL's. Ex:

* http://localhost:8080/api/client - HTTP POST (Chama o serviço de gravar o cliente)
* http://localhost:8080/api/client - HTTP PUT (Chama o serviço de atualizar o cliente)
* http://localhost:8080/api/client - HTTP GET (Chama o serviço que recupera todos os clientes) e assim por diante.

Para realizar as chamadas no Angular de serviços rest usamos o módulo **Http* que está declarado no construtor da classe ClientService:
```java
constructor(private http: Http) {}
```
Através desta variável **http** podemos chamar as operações PUT, POST e etc. Um ponto importante aqui é que usamos o _JSON_ como padrão de mensagens, portanto o cabeção da requisição precisa ser difinido como _JSON_, conforme já definido na classe:

```java
private headers = new Headers({'Content-Type': 'application/json'});
```
E como dito anteriormente, todo retorno de serviços nesta aplicação, está sendo tratado como uma _Promise_.

#### Roteamento
O roteamento da aplicação está sendo realizado no componente **app-routing.module.ts**. Basta verificar neste componente, todos os roteamentos que a aplicação já possui para relizar a tarefa de UPDATE. 